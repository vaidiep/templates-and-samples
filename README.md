# Templates and Samples

CSV file templates, samples and other misc. information for working with the Pledge platform

## Getting started

To learn more, visit the [Pledge Help Center](https://help.pledge.io/en/collections/3158785-importing-your-data)

